﻿using System;
using System.Diagnostics;
namespace timer
{
    public class Shutdown
    {
        public void Start(Int64 Second)
        {
            Process.Start("shutdown", String.Format("/s /t {0}", Second));
        }

        public void Cancel()
        {
            Process.Start("shutdown", "/a");
        }

    }
}
