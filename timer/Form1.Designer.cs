﻿namespace timer
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.go_bt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Cancel_bt = new System.Windows.Forms.Button();
            this.dateTime_picker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // go_bt
            // 
            this.go_bt.Location = new System.Drawing.Point(114, 30);
            this.go_bt.Name = "go_bt";
            this.go_bt.Size = new System.Drawing.Size(137, 23);
            this.go_bt.TabIndex = 1;
            this.go_bt.Text = "Пуск";
            this.go_bt.UseVisualStyleBackColor = true;
            this.go_bt.Click += new System.EventHandler(this.go_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Через сколько минут выключить компьютер?";
            // 
            // Cancel_bt
            // 
            this.Cancel_bt.Location = new System.Drawing.Point(12, 59);
            this.Cancel_bt.Name = "Cancel_bt";
            this.Cancel_bt.Size = new System.Drawing.Size(239, 23);
            this.Cancel_bt.TabIndex = 3;
            this.Cancel_bt.Text = "Отмена таймеров";
            this.Cancel_bt.UseVisualStyleBackColor = true;
            this.Cancel_bt.Click += new System.EventHandler(this.Cancel_bt_Click);
            // 
            // dateTime_picker
            // 
            this.dateTime_picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTime_picker.Location = new System.Drawing.Point(12, 31);
            this.dateTime_picker.Name = "dateTime_picker";
            this.dateTime_picker.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dateTime_picker.ShowUpDown = true;
            this.dateTime_picker.Size = new System.Drawing.Size(96, 20);
            this.dateTime_picker.TabIndex = 4;
            this.dateTime_picker.Value = new System.DateTime(2014, 7, 18, 0, 0, 0, 0);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(262, 92);
            this.Controls.Add(this.dateTime_picker);
            this.Controls.Add(this.Cancel_bt);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.go_bt);
            this.Name = "Form1";
            this.Text = "Таймер выключения ПК";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button go_bt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Cancel_bt;
        private System.Windows.Forms.DateTimePicker dateTime_picker;
    }
}

