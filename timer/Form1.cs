﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace timer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Shutdown shutdown = new Shutdown();

        private void go_Click(object sender, EventArgs e)
        {
            Int64 second = Convert.ToInt64(GetSeconds(dateTime_picker.Text));
            shutdown.Start(second);
        }

        private double GetSeconds(string time)
        {
            DateTime _time = Convert.ToDateTime(time);
            return new TimeSpan(0, _time.Hour, _time.Minute, _time.Second).TotalSeconds;
        }

        private void Cancel_bt_Click(object sender, EventArgs e)
        {
            shutdown.Cancel();
        }

    }
}
